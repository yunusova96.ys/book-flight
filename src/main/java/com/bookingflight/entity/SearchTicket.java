package com.bookingflight.entity;

import java.time.LocalDate;

public class SearchTicket {

//    The user is prompted to enter the following information: destination, date,
//    number of people (how many tickets to buy).
//

    private  String destination;
    private LocalDate date;
    private  int passengerCount;

    public SearchTicket(String destination, LocalDate date, int passengerCount) {
        this.destination = destination;
        this.date = date;
        this.passengerCount = passengerCount;
    }

    public SearchTicket() {
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(int passengerCount) {
        this.passengerCount = passengerCount;
    }

    @Override
    public String toString() {
        return "SearchTicket{" +
                "destination='" + destination + '\'' +
                ", date=" + date +
                ", passengerCount=" + passengerCount +
                '}';
    }
}
