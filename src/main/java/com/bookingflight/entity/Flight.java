package com.bookingflight.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Flight implements Serializable {
    //id date time from to seats
    private  int flightId;
    private LocalDate date;
    private  int seats;
    private  String from;
    private  String to;

    private static long serialVersionUID = 1L;

    public Flight() {
    }

    public Flight(int flightId, LocalDate date, int seats, String from, String to) {
        this.flightId = flightId;
        this.date = date;
        this.seats = seats;
        this.from = from;
        this.to = to;
    }

    public String show() {
        return String.format("|Id: %d\t|\tFrom  '%s'   to   '%s'   %s  empty seats: %d  ", flightId, from, to, date, seats);
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightId=" + flightId +
                ", date=" + date +
                ", seats=" + seats +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }



    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Flight(int flightId, LocalDate date, LocalDateTime time, int seats, String from, String to) {
        this.flightId = flightId;
        this.date = date;
        this.seats = seats;
        this.from = from;
        this.to = to;
    }

    public String getDestination(){
        return from+to;
    }

    public String getDetails(){
        return "destiantion"+getDestination()+
                "\nid"+flightId+
                "\ndate"+date+
                "\nseats"+seats;

    }
}
