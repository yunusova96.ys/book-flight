package com.bookingflight.entity;

import java.io.Serializable;
import java.util.List;

public class Booking implements Serializable {
    private static long serialVersionUID = 1L;

    private  int id;
    private  int flightId;
    private  int userId;
    private List<Passenger> passengerList;

    public Booking(int id, int flightId, int userId, List<Passenger> passengerList) {
        this.id = id;
        this.flightId = flightId;
        this.userId = userId;
        this.passengerList = passengerList;
    }


    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", flightId=" + flightId +
                ", userId=" + userId +
                ", passengerList=" + passengerList +
                '}';
    }

    public String show() {
        return String.format(" Id: %d    userId: '%d'   userId:   '%d'  passengers: %s   ", id,flightId,userId,passengerList.toString());
    }


    public Booking() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Passenger> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<Passenger> passengerList) {
        this.passengerList = passengerList;
    }
}
