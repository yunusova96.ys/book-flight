package com.bookingflight.entity;

import java.util.List;

public class FlightSearchResult {
    private final boolean isConnnected;
    private final  List<Flight> listOfResult;

    public FlightSearchResult(boolean isConnnected, List<Flight> listOfResult) {
        this.isConnnected = isConnnected;
        this.listOfResult = listOfResult;
    }



    public boolean isConnnected() {
        return isConnnected;
    }

    public List<Flight> getListOfResult() {
        return listOfResult;
    }
}
