package com.bookingflight.entity;

import java.io.Serializable;

public class User implements Serializable {
//    id name surname passworddd
    private int userId;
    private String name;
    private String surName;
    private String userName;
    private String password;
    private String email;
    private String phoneNumber;//diff between abs interface
    private static long serialVersionUID = 1L;

    public User() {
    }

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String show() {
        return String.format(" Id: %d   Name:  %s   surName:  %s   userName: %s password: %s email: %s phoneNumber: %s ", userId,name,surName,userName,password,email,phoneNumber);
    }

    @Override
    public String
    toString() {
        return "User{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    public User(int userId, String name, String surName, String userName, String password, String email, String phoneNumber) {
        this.userId = userId;
        this.name = name;
        this.surName = surName;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public  String getFullname(){
        return  name+surName;
    }
    public  String getDetails(){
        return "id"+ userId+
                "\nfullName"+ getFullname()+
                "\nusername"+userName+
                "\nemail"+ email+
                "\nphoneNumber"+phoneNumber;
    }
}
