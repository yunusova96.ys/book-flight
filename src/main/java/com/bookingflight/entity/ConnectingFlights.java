package com.bookingflight.entity;

import java.util.Objects;

public class ConnectingFlights {
    private Flight firstFlight;
    private  Flight secondFlight;

    public ConnectingFlights() {
    }

    public ConnectingFlights(Flight firstFlight, Flight secondFlight) {
        this.firstFlight = firstFlight;
        this.secondFlight = secondFlight;
    }

    public Flight getFirstFlight() {
        return firstFlight;
    }

    public void setFirstFlight(Flight firstFlight) {
        this.firstFlight = firstFlight;
    }

    public Flight getSecondFlight() {
        return secondFlight;
    }

    public void setSecondFlight(Flight secondFlight) {
        this.secondFlight = secondFlight;
    }

    @Override
    public String toString() {
        return "ConnectingFlights{" +
                "firstFlight=" + firstFlight +
                ", secondFlight=" + secondFlight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConnectingFlights that = (ConnectingFlights) o;
        return Objects.equals(firstFlight, that.firstFlight) &&
                Objects.equals(secondFlight, that.secondFlight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstFlight, secondFlight);
    }
}
