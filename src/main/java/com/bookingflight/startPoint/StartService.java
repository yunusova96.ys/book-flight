package com.bookingflight.startPoint;

import com.bookingflight.menu.Menu;
import com.bookingflight.menu.MenuBuilder;
import com.bookingflight.utility.LogUtility;

public class StartService {
    public static void main(String[] args) {
        LogUtility.addLog("StartService","Starting new proje",LogUtility.LogTypes.INFO);

        Menu menu = new MenuBuilder().createStartMenu();

        LogUtility.addLog("StartService","Menu built",LogUtility.LogTypes.INFO);
        while(menu!=null){
            LogUtility.addLog("StartService","Running menu command",LogUtility.LogTypes.INFO);
            menu = menu.command();
        }

    }
}
