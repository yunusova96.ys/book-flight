package com.bookingflight.controller.impl;

import com.bookingflight.entity.Booking;
import com.bookingflight.entity.Flight;
import com.bookingflight.entity.FlightSearchResult;
import com.bookingflight.entity.Passenger;
import com.bookingflight.service.BookingService;
import com.bookingflight.service.FlightService;
import com.bookingflight.service.GetInput;
import com.bookingflight.utility.UserUtility;

import java.util.List;
import java.util.Optional;

public class BookingController implements GetInput {
    private static BookingController instance;

    public static BookingController getInstance() {
        if (instance == null) {
            instance = new BookingController();
        }
        return instance;
    }

    private BookingService bookingService;
    private FlightService flightService;

    public BookingController() {
        bookingService = new BookingService();
        flightService = new FlightService();
    }

    public Flight getFlight(int idFlight) {
        try {
            return FlightController.getInstance().getFlight(idFlight);
        } catch (Exception e) {
            e.printStackTrace();
            return new Flight();
        }
    }

    private enum ReserveType {
        RESERVE, UNRESERVE;
    }

    public boolean reserveSeats(int idFlight, int numberOfSeats, ReserveType reserveType) {
        Flight flight = getFlight(idFlight);
        switch (reserveType) {
            case RESERVE:
                flight.setSeats(flight.getSeats() - numberOfSeats);
                break;
            case UNRESERVE:
                flight.setSeats(flight.getSeats() + numberOfSeats);

        }

        return FlightController.getInstance().update(flight);
    }


    public int getSeats() throws Exception {
        return getIntData("How many tickets to buy:");

    }

    public int getConnectedFlightAnswer() {
        return getIntData("Do you want to book these flights? (Yes : 1 | No : 0");
    }

    public boolean startBooking(FlightSearchResult result) throws Exception {

        int seats = getSeats();
        int idFlight;
        if(!result.isConnnected()){
            idFlight = bookingService.getFlight();

            return createBooking(seats,idFlight, bookingService.getPassengerList(seats));
        }else{
            int answer = getConnectedFlightAnswer();
            if(answer==1){
                //booking all flight
                List<Passenger> list = bookingService.getPassengerList(seats);
                for (Flight flight : result.getListOfResult()) {
                    createBooking(seats,flight.getFlightId(), list);//done
                }
                return true;
            }else{
                return false;
            }
        }
    }

    private boolean createBooking(int seats, int idFlight, List<Passenger> passengersList){
        if(seats!=0&&idFlight!=0) {
            bookingService.createBooking(seats, idFlight, UserUtility.getInstance().getLoggedUser(), passengersList);
            return reserveSeats(idFlight, seats, ReserveType.RESERVE);
        }else{
            return false;
        }

    }
    public int getBookingID() throws Exception {
        return getIntData("enter booking id for canceling:");

    }

    public boolean cancelBooking() throws Exception {
        int bookingID = getBookingID();
        if (bookingID==0){
            return false;
        }
        Optional<Booking> bookingInfo = bookingService.getBookingInfo(bookingID);
        if(bookingInfo.isPresent()){
            boolean result = bookingService.cancelBooking(bookingInfo.get());
            if(result){
                return reserveSeats(
                        bookingInfo.get().getFlightId(),
                        bookingInfo.get().getPassengerList().size(),
                        ReserveType.UNRESERVE
                );
            }else{
                return false;
            }
        }else{
            System.out.println("No booking has found with given id");
            return false;
        }
    }

}
