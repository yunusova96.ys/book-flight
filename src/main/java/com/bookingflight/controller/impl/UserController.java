package com.bookingflight.controller.impl;

import com.bookingflight.entity.User;
import com.bookingflight.service.GetInput;
import com.bookingflight.service.UserService;

import java.util.ArrayList;
import java.util.Collection;

public class UserController implements GetInput {
    private  static  UserController instance;
     public static UserController getInstance(){
         if (instance==null){
           instance=  new UserController();
         }
         return  instance;
     }
     UserService userService;

    public UserController() {
         userService = new UserService();
    }


     public  void createUser(){
         try {
             User user = new User();
             user.setUserId((Integer.parseInt(System.currentTimeMillis() % 1000000000+"")));
             user.setName(getStringData("Name\t\t:\t\t"));
             user.setSurName(getStringData("Surname\t\t:\t\t"));
             user.setUserName(getStringData("Username\t\t:\t\t"));
             user.setPassword(getStringData("Password\t\t:\t\t"));
             user.setEmail(getStringData("User mail\t\t:\t\t"));
             user.setPhoneNumber(getStringData("Phone\t\t:\t\t"));
              userService.createUser(user);
         } catch (Exception e) {
             System.out.println("Couldnt create user");
         }
     }

     public  boolean login(){
         try {
             String username = getStringData("Please enter user login details\nUsername\t\t:\t\t");
             String password = getStringData("Password\t\t:\t\t");
             return userService.login(username,password);
         } catch (Exception e) {
             return  false;
         }
     }

     public Collection<User> getAll(){
         try {
             return userService.getAll();
         } catch (Exception e) {
             return new ArrayList<>();
         }
     }

}
