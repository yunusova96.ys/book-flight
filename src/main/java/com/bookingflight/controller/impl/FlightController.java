package com.bookingflight.controller.impl;

import com.bookingflight.entity.ConnectingFlights;
import com.bookingflight.entity.Flight;
import com.bookingflight.entity.FlightSearchResult;
import com.bookingflight.service.FlightService;
import com.bookingflight.utility.LogUtility;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FlightController {
    private static FlightController instance;

    public static FlightController getInstance() {
        if (instance == null) {
            instance = new FlightController();
        }
        return instance;
    }

    private FlightService flightService;

    public FlightController() {
        flightService = new FlightService();
    }

    public void creatAllFlights() {
        try {
            flightService.createAllFlights();
        } catch (Exception e) {
            LogUtility.addLog("FlightController", "Error  crating All flghts", LogUtility.LogTypes.ERROR);
        }
    }


    public Flight getFlight(int id) {

        try {
            return flightService.getFlight(id);
        } catch (Exception e) {
            LogUtility.addLog("FlightController", "Error  getting by id flghts", LogUtility.LogTypes.ERROR);
            return null;
        }

    }

    public FlightSearchResult getAllByParam() {
        try {
            flightService.getStartLine();
            String from = flightService.getFrom();
            String destination = flightService.getDestination();
            LocalDate date = flightService.getFlightDate();
            List<Flight> allFlightByParameter = flightService.getAllFlightByParameter(
                    from,destination,date);

            if (allFlightByParameter.isEmpty()){
                Optional<ConnectingFlights> connectingFlight = flightService.getConnectingFlight(from, destination, date);
                if(connectingFlight.isPresent()){
                    System.out.println("No exact Flights has found with given parameters\nYou can use our connecting flights...");
                    ArrayList<Flight> list = new ArrayList<>();
                    list.add(connectingFlight.get().getFirstFlight());
                    list.add(connectingFlight.get().getSecondFlight());
                return new FlightSearchResult(true,list);
                }
                return null;
            }
            else {
                return new FlightSearchResult(false,allFlightByParameter);
            }
        } catch (Exception e) {
            LogUtility.addLog("Error","Error getting flight by param", LogUtility.LogTypes.ERROR);
            return  null;
        }
    }

    public List<Flight> getAll() {
        return flightService.getAllFlight();
    }

    public List<Flight> getAllToday(){
        try {
            return flightService.getAllFlightToday();
        } catch (Exception e) {
            System.out.println("No flightlist has found for today");
            return  new ArrayList<>();
        }

    }

    public Optional<Flight> getFInfoById() throws Exception{

        Optional<Flight> flight = flightService.getOneFlightInfoById();
        if(flight.isPresent()){
            return flight;
        }

        System.out.println("No flight info has found with given id");
            return Optional.empty();

    }

    public boolean update(Flight flight){
        try {
            return flightService.update(flight);
        } catch (Exception e) {
            System.out.println("No such Flight has found for updating...");
            return  false;
        }
    }


}
