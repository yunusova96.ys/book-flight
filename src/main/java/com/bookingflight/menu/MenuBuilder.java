package com.bookingflight.menu;

import com.bookingflight.submenu.*;

public class MenuBuilder {

    public Menu createStartMenu(){
        Menu menu = new StartMenu(null,null);
        menu.addMenu(createLoginMenu(menu));
        menu.addMenu(createRegisterMenu(menu));
        menu.addMenu(new ExitMenu());

        return menu;
    }

    public Menu createLoginMenu(Menu previousMenu){//todo sorush
        Menu menu = new Login(previousMenu,createDashboard(previousMenu));

        return menu;
    }

    public Menu createDashboard(Menu previousMenu){
        Menu menu = new MenuDashboard(previousMenu);
        menu.addMenu(new ShowAllFlights(menu));
        menu.addMenu(new ShowAllFlightsToday(menu));
        menu.addMenu(new SearchForAFlight(menu));
        menu.addMenu(new MakeABooking(menu));
        menu.addMenu(new ShowMyBooking(menu));
        menu.addMenu(new CancelBooking(menu));
        menu.addMenu(new LogOutMenu(previousMenu));

        return menu;
    }

    public Menu createRegisterMenu(Menu nextMenu){
        Menu menu = new RegisterMenu(nextMenu);
        return menu;
    }

}
