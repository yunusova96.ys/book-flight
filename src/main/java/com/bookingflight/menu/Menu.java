package com.bookingflight.menu;

import com.bookingflight.utility.LogUtility;

import java.util.InputMismatchException;
import java.util.Scanner;

public interface Menu {

    Scanner scanner = new Scanner(System.in);

    public void addMenu(Menu menu);
    public Menu getMenu(int enteredMenuItem);
    public Menu command();
    public Menu getPreviousMenu();
    public Menu getNextMenu();

//    default void showMenu(MenuEnum menuEnum){
//        System.out.print(menuEnum.getMenuInfo());
//    }
    default void errorSelectedMenu(){
        System.out.println("Be careful with your choice");
    }
    default int getIntData(String menu){
        System.out.print(menu);
        try {
            return scanner.nextInt();
        }catch (InputMismatchException e){
            LogUtility.addLog("Menu","get integer : "+e,LogUtility.LogTypes.ERROR);
            scanner.nextLine();
            return -1;
        }
    }
}
