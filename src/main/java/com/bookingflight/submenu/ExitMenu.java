package com.bookingflight.submenu;

import com.bookingflight.menu.Menu;
import com.bookingflight.utility.LogUtility;

public class ExitMenu implements Menu {
    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu command() {
        LogUtility.addLog("Exit menu","Exited from app",LogUtility.LogTypes.INFO);

        System.out.println("Bye bye");
        return null;
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return null;
    }
}
