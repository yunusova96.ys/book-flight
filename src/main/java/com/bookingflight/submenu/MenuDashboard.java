package com.bookingflight.submenu;

import com.bookingflight.menu.Menu;
import com.bookingflight.utility.AllFlightsMenuPrintUtility;
import com.bookingflight.utility.LogUtility;

import java.util.ArrayList;
import java.util.List;

public class MenuDashboard implements Menu {


    private final Menu previousMenu;
//    private MenuEnum menuEnum;
    private List<Menu> listOfMenu;

    public MenuDashboard(Menu previousMenu) {
//        menuEnum = MenuEnum.DASHBOARD;
        this.previousMenu = previousMenu;
        listOfMenu = new ArrayList<>();
    }

    @Override
    public void addMenu(Menu menu) {
        listOfMenu.add(menu);
    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return enteredMenuItem < 1 || enteredMenuItem > 71 ? null : listOfMenu.get(enteredMenuItem - 1);
    }

    @Override
    public Menu getPreviousMenu() {
        return previousMenu;
    }

    @Override
    public Menu getNextMenu() {
        return null;
    }


    public void showMenuInfo() {
        AllFlightsMenuPrintUtility instance = AllFlightsMenuPrintUtility.getInstance();
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("No", 4));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("Dashboard", 20));
        AllFlightsMenuPrintUtility.getInstance().printHeaders();
        AllFlightsMenuPrintUtility.getInstance().printData("1.", "Show All Flights");
        AllFlightsMenuPrintUtility.getInstance().printData("2.", "Flights of Today");
        AllFlightsMenuPrintUtility.getInstance().printData("3.", "Search for a Flight");
        AllFlightsMenuPrintUtility.getInstance().printData("4.", "Make a booking");
        AllFlightsMenuPrintUtility.getInstance().printData("5.", "Show my bookings");
        AllFlightsMenuPrintUtility.getInstance().printData("6.", "Cancel  bookings");
        AllFlightsMenuPrintUtility.getInstance().printData("7.", "Log out");
        AllFlightsMenuPrintUtility.getInstance().printFooter();

        AllFlightsMenuPrintUtility.getInstance().refreshAll();
    }

    @Override
    public Menu command() {

        showMenuInfo();
        LogUtility.addLog("Dashboard", "Dashboard menu has shown", LogUtility.LogTypes.INFO);
        int enteredMenuItem = 0;
        enteredMenuItem = getIntData("->");
        Menu menu = getMenu(enteredMenuItem);
        if (menu == null) {
            errorSelectedMenu();
            return this;
        } else {
            return menu;
        }
    }
}
