package com.bookingflight.submenu;

import com.bookingflight.entity.Flight;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.FlightService;
import com.bookingflight.utility.AllFlightsMenuPrintUtility;
import com.bookingflight.utility.LogUtility;

import java.util.List;
import java.util.function.Consumer;

public class ShowAllFlights implements Menu {
//    private  MenuEnum menuEnum;
    private  Menu nextMenu;

    public ShowAllFlights(Menu nextMenu) {
        this.nextMenu = nextMenu;
//        menuEnum = MenuEnum.SHOW_FLIGHTS;
    }

    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu command() {
        AllFlightsMenuPrintUtility instance = AllFlightsMenuPrintUtility.getInstance();
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ID",6));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ORIGIN",40));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("DESTINATION",40));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("DATE",12));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("SEATS",6));

        AllFlightsMenuPrintUtility.getInstance().printHeaders();
        FlightService service = new FlightService();
        List<Flight> list = service.getAllFlight();
        list.forEach(new Consumer<Flight>() {
            @Override
            public void accept(Flight flight) {
                AllFlightsMenuPrintUtility.getInstance().printData(
                        flight.getFlightId()+"",
                        flight.getFrom(),
                        flight.getTo(),
                        flight.getDate().toString(),
                        flight.getSeats()+"");
            }
        });

        AllFlightsMenuPrintUtility.getInstance().printFooter();
        AllFlightsMenuPrintUtility.getInstance().refreshAll();

        LogUtility.addLog("Show all flights menu","Getting all flights",LogUtility.LogTypes.INFO);
        return getNextMenu();
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
