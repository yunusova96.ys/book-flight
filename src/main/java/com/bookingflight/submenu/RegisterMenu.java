package com.bookingflight.submenu;

import com.bookingflight.controller.impl.UserController;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.UserService;
import com.bookingflight.utility.LogUtility;

public class RegisterMenu implements Menu {


//    private MenuEnum menuEnum;
    private Menu previousMenu;
    private Menu nextMenu;

    public RegisterMenu(Menu nextMenu){
        this.nextMenu = nextMenu;
//        menuEnum = MenuEnum.SIGN_UP;
    }

    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu command() {
        UserController.getInstance().createUser();
        LogUtility.addLog("Register menu","new User has created...",LogUtility.LogTypes.INFO);
        return getNextMenu();
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
