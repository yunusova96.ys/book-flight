package com.bookingflight.submenu;

import com.bookingflight.menu.Menu;
import com.bookingflight.utility.AllFlightsMenuPrintUtility;
import com.bookingflight.utility.LogUtility;

import java.util.ArrayList;
import java.util.List;

public class StartMenu implements Menu {

    private List<Menu> listOfMenus;
//    private MenuEnum menuEnum;
    private Menu previousMenu;
    private Menu nextMenu;

//    public StartMenu(Scanner scanner,Menu previousMenu) {
//        this.listOfMenus = new ArrayList<>();
//        menuEnum = MenuEnum.START_MENU;
//        this.scanner = scanner;
//        this.previousMenu = previousMenu;
//    }
    public StartMenu(Menu previousMenu, Menu nextMenu) {
        this.listOfMenus = new ArrayList<>();
//        menuEnum = MenuEnum.START_MENU;
        this.previousMenu = previousMenu;
        this.nextMenu = nextMenu;
    }

    @Override
    public void addMenu(Menu menu) {
        listOfMenus.add(menu);
    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return (enteredMenuItem<1||enteredMenuItem>3)?null:listOfMenus.get(enteredMenuItem-1);
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return null;
    }

    private void showMenuInfo(){
        AllFlightsMenuPrintUtility instance = AllFlightsMenuPrintUtility.getInstance();
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("No",4));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("Start menu",20));
        AllFlightsMenuPrintUtility.getInstance().printHeaders();
        AllFlightsMenuPrintUtility.getInstance().printData("1","Login");
        AllFlightsMenuPrintUtility.getInstance().printData("2","Register");
        AllFlightsMenuPrintUtility.getInstance().printData("3","Exit");
//        AllFlightsMenuPrintUtility.getInstance().printHeaders();
        AllFlightsMenuPrintUtility.getInstance().printFooter();

        AllFlightsMenuPrintUtility.getInstance().refreshAll();
    }

    @Override
    public Menu command() {
        LogUtility.addLog("Start menu","visited...",LogUtility.LogTypes.INFO);

//        showMenu(menuEnum);
        showMenuInfo();
        int enteredMenuItem = 0;
            enteredMenuItem = getIntData("->");

        Menu menu = getMenu(enteredMenuItem);
        if(menu == null){
            errorSelectedMenu();
            return this;
        }else{
            return menu;
        }
    }

}
