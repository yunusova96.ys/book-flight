package com.bookingflight.submenu;

import com.bookingflight.entity.User;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.UserService;
import com.bookingflight.utility.LogUtility;
import com.bookingflight.utility.UserUtility;

public class LogOutMenu implements Menu {

    private Menu nextMenu;

    public LogOutMenu(Menu nextMenu) {
        this.nextMenu = nextMenu;
    }

    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu command() {
        UserUtility.getInstance().setLoggedUser(null);
        LogUtility.addLog("Log out menu","User has logged out",LogUtility.LogTypes.INFO);

        return nextMenu;//StartMenu
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return null;
    }
}
