package com.bookingflight.submenu;

import com.bookingflight.controller.impl.BookingController;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.BookingService;
import com.bookingflight.utility.LogUtility;

public class CancelBooking implements Menu {

//    private MenuEnum menuEnum;
    private Menu nextMenu;

    public CancelBooking(Menu nextMenu){
        this.nextMenu = nextMenu;
//        menuEnum = MenuEnum.CANCEL_BOOKING;
    }
    @Override
    public void addMenu(Menu menu) {

    }



    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu command() {
        LogUtility.addLog("Cancel Booking","booking cancelled...",LogUtility.LogTypes.INFO);

        boolean cancelled = false;
        try {
            cancelled = BookingController.getInstance().cancelBooking();
        if (cancelled){
            System.out.println("Cancelled succesfully...");
        }else {
            errorSelectedMenu();
        }
        } catch (Exception e) {
            return this;
        }
        return getNextMenu();
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
