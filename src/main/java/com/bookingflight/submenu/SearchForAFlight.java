package com.bookingflight.submenu;

import com.bookingflight.controller.impl.FlightController;
import com.bookingflight.entity.Flight;
import com.bookingflight.entity.FlightSearchResult;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.FlightService;
import com.bookingflight.utility.AllFlightsMenuPrintUtility;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class SearchForAFlight implements Menu {

    //    private MenuEnum menuEnum;
    private Menu nextMenu;

    public SearchForAFlight(Menu nextMenu) {
        this.nextMenu = nextMenu;
//        menuEnum = MenuEnum.SEARCH_FLIGHT;
    }


    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    private void printData(Flight flight){
        AllFlightsMenuPrintUtility instance = AllFlightsMenuPrintUtility.getInstance();
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ID", 6));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ORIGIN", 40));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("DESTINATION", 40));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("DATE", 12));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("SEATS", 6));

        AllFlightsMenuPrintUtility.getInstance().printHeaders();
        AllFlightsMenuPrintUtility.getInstance().printData(
                flight.getFlightId() + "",
                flight.getFrom(),
                flight.getTo(),
                flight.getDate().toString(),
                flight.getSeats() + "");
        AllFlightsMenuPrintUtility.getInstance().printFooter();
        AllFlightsMenuPrintUtility.getInstance().refreshAll();
    }

    @Override
    public Menu command() {
        Optional<Flight> flight;
        try {
            flight = FlightController.getInstance().getFInfoById();
            flight.ifPresent(this::printData);
            return nextMenu;
        } catch (Exception e) {
            System.out.println("coulnt find flight");
            return this;
        }
    }
    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }

    private List<Flight> menuCommand() {
        FlightSearchResult result = FlightController.getInstance().getAllByParam();
        return result.getListOfResult();
    }
}

