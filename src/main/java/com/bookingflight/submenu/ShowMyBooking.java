package com.bookingflight.submenu;

import com.bookingflight.entity.Booking;
import com.bookingflight.entity.Passenger;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.BookingService;
import com.bookingflight.utility.AllFlightsMenuPrintUtility;
import com.bookingflight.utility.LogUtility;

import java.util.List;
import java.util.function.Consumer;

public class ShowMyBooking implements Menu {
//    private MenuEnum menuEnum;
    private Menu nextMenu;

    public ShowMyBooking(Menu nextMenu){
        this.nextMenu = nextMenu;
//        menuEnum = MenuEnum.SHOW_MY_BOOKING;
    }
    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }
    public  void  showMenu(){
        AllFlightsMenuPrintUtility instance = AllFlightsMenuPrintUtility.getInstance();
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ID",12));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("Flight ID",12));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("User ID",12));
        instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("Passengers",60));
    }
    @Override
    public Menu command() {
        showMenu();
        AllFlightsMenuPrintUtility.getInstance().printHeaders();

        BookingService service = new BookingService();
       List<Booking> list = (List<Booking>)service.getAllByUserId();
       list.forEach(new Consumer<Booking>() {
//
           @Override
           public void accept(Booking booking) {
                String passengers = "";
               for (Passenger passenger : booking.getPassengerList()) {
                   passengers = passengers
                           .concat(passenger.getName())
                           .concat(" ")
                           .concat(passenger.getSurname())
                           .concat(",");
               }
               passengers = passengers.substring(0,passengers.length()-1);

               AllFlightsMenuPrintUtility.getInstance().printData(
                       booking.getId() + "",
                       booking.getFlightId() + "",
                       booking.getUserId() + "",
                       passengers
               );

           }

       });
        AllFlightsMenuPrintUtility.getInstance().printFooter();
        AllFlightsMenuPrintUtility.getInstance().refreshAll();
        LogUtility.addLog("ShowMyBooking menu","User has got all bookings",LogUtility.LogTypes.INFO);

        return getNextMenu();
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
