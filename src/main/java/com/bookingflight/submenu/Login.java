package com.bookingflight.submenu;

import com.bookingflight.controller.impl.UserController;
import com.bookingflight.entity.User;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class Login implements Menu {

//    private MenuEnum menuEnum;
    private List<Menu> listOfMenu;
    private Menu previousMenu;
    private Menu nextMenu;

    public Login(Menu previousMenu, Menu nextMenu) {
        listOfMenu = new ArrayList<>();
//        menuEnum = MenuEnum.LOG_IN;
        this.previousMenu = previousMenu;
        this.nextMenu = nextMenu;
    }

    @Override
    public void addMenu(Menu menu) {
        listOfMenu.add(menu);
    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu getPreviousMenu() {
        return previousMenu;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }

    @Override
    public Menu command() {
//        showMenu(menuEnum);

        if(menuCommand()){
            return getNextMenu();
        }else{
            return getPreviousMenu();
        }
    }

    private boolean menuCommand(){
        List<User> list = (List<User>) UserController.getInstance().getAll();
//        System.out.println(list);
        return UserController.getInstance().login();
    }





}
