package com.bookingflight.submenu;

import com.bookingflight.controller.impl.BookingController;
import com.bookingflight.controller.impl.FlightController;
import com.bookingflight.entity.Flight;
import com.bookingflight.entity.FlightSearchResult;
import com.bookingflight.menu.Menu;
import com.bookingflight.service.BookingService;
import com.bookingflight.service.FlightService;
import com.bookingflight.utility.AllFlightsMenuPrintUtility;
import com.bookingflight.utility.LogUtility;

import java.util.List;
import java.util.function.Consumer;

public class MakeABooking implements Menu {
//    private MenuEnum menuEnum;
    private Menu nextMenu;

    public MakeABooking(Menu nextMenu){
        this.nextMenu = nextMenu;
//        menuEnum = MenuEnum.MAKE_BOOKING;
    }
    @Override
    public void addMenu(Menu menu) {

    }

    @Override
    public Menu getMenu(int enteredMenuItem) {
        return null;
    }

    @Override
    public Menu command() {
        FlightSearchResult result = FlightController.getInstance().getAllByParam();
        if(result!=null) {
            AllFlightsMenuPrintUtility instance = AllFlightsMenuPrintUtility.getInstance();
            instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ID", 6));
            instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("ORIGIN", 40));
            instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("DESTINATION", 40));
            instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("DATE", 12));
            instance.addHeader(new AllFlightsMenuPrintUtility.HeaderInfo("SEATS", 6));

            AllFlightsMenuPrintUtility.getInstance().printHeaders();
            result.getListOfResult().forEach(new Consumer<Flight>() {
                @Override
                public void accept(Flight flight) {
                    AllFlightsMenuPrintUtility.getInstance().printData(
                            flight.getFlightId() + "",
                            flight.getFrom(),
                            flight.getTo(),
                            flight.getDate().toString(),
                            flight.getSeats() + "");
                }
            });

            AllFlightsMenuPrintUtility.getInstance().printFooter();
            AllFlightsMenuPrintUtility.getInstance().refreshAll();
        }
//

        if(result==null|| result.getListOfResult().isEmpty()){
            System.out.println("Sorry :( no flight has found with your given criteria");
        }else {

            try {
                System.out.println("starting booking...");
                BookingController.getInstance().startBooking(result);
            } catch (Exception e) {
                System.out.println("e : "+e);
                return getNextMenu();
            }

            System.out.println("Booking successfully done...");
            LogUtility.addLog("Make a booking", "User has made a booking...", LogUtility.LogTypes.INFO);
        }
        return getNextMenu();
    }

    @Override
    public Menu getPreviousMenu() {
        return null;
    }

    @Override
    public Menu getNextMenu() {
        return nextMenu;
    }
}
