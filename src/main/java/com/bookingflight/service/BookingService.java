package com.bookingflight.service;

import com.bookingflight.dao.Dao;
import com.bookingflight.dao.DaoFactory;
import com.bookingflight.dao.iml.BookingDao;
import com.bookingflight.entity.Booking;
import com.bookingflight.entity.Passenger;
import com.bookingflight.entity.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class BookingService implements GetInput {
    private final Dao<Booking> bookingDao;

    public BookingService() {
        bookingDao = DaoFactory.getDAO(DaoFactory.DaoNames.BOOKING);
        ;
    }

    public int getFlight() throws Exception {

        return getIntData("Please enter details for booking\nflight id\t:\t");

    }

    public boolean createBooking(int seats, int idFlight, User loggedUser, List<Passenger> passengersList) {
        Booking booking = new Booking();
        booking.setId(createBookingID());
        booking.setFlightId(idFlight);
        booking.setUserId(loggedUser.getUserId());

        booking.setPassengerList(passengersList);
        bookingDao.create(booking);
        return true;
    }


    private int createBookingID() {
        return Integer.parseInt(System.currentTimeMillis() % 1000000000 + "");
    }


    public List<Passenger> getPassengerList(int seats) {
        ArrayList<Passenger> passengerList = new ArrayList<>();
        for (int i = 0; i < seats; i++) {
            Passenger passenger = new Passenger();
            passenger.setName(getStringData("name\t:\t"));
            passenger.setSurname(getStringData("surname\t:\t"));
            passengerList.add(passenger);
        }
        return passengerList;
    }


    public Collection<Booking> getAllByUserId() {
        return ((BookingDao) bookingDao).getAllByLoggedUser();
    }



    public Optional<Booking> getBookingInfo(int idBooking) {
        return bookingDao.get(idBooking);
    }

    public boolean cancelBooking(Booking booking) {
        return bookingDao.delete(booking.getId());

    }
}

