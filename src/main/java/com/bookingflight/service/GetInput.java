package com.bookingflight.service;

import java.util.Scanner;

public interface GetInput {
    Scanner scanner = new Scanner(System.in);

    default String getStringDataAsLine(String menu) {

        System.out.print(menu);
        return scanner.nextLine();
    }

    default String getStringData(String menu) {
        try {
            System.out.print(menu);
            return scanner.next();
        } catch (Exception e) {
            return "be careful with your input";
        }
    }

    default int getIntData(String menu) {
        try {
            System.out.print(menu);
            return scanner.nextInt();
        } catch (Exception e) {
            scanner.next();
            System.out.println("HEYYYY...Oopsss u have to enter integerr");
            return 0;
        }
    }
}
