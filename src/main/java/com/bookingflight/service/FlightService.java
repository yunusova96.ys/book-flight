package com.bookingflight.service;

import com.bookingflight.dao.Dao;
import com.bookingflight.dao.DaoFactory;
import com.bookingflight.dao.iml.FlightDao;
import com.bookingflight.entity.ConnectingFlights;
import com.bookingflight.entity.Flight;
import com.bookingflight.utility.LogUtility;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FlightService implements GetInput {
    private final Dao<Flight> flightDao;

    public FlightService() {
        flightDao = DaoFactory.getDAO(DaoFactory.DaoNames.FLIGHT);
    }

    public void createAllFlights() {
        FlightDao dao = new FlightDao();
        for (int i = 0; i < 50; i++) {
            dao.create(createRandomFlights());
        }
    }

    public Flight createRandomFlights() {
        Flight flight = new Flight();
        flight.setFlightId(getRandomFlightId());
        flight.setFrom(getRandomCity());
        flight.setTo(getRandomCity());
        flight.setDate(getRandomDay());
        flight.setSeats(getRandomSeats());

        return flight;
    }

    public int getRandomFlightId() {
        int flightId = (int) (Math.random() * 1000 + 2000);
        return flightId;
    }

    public String getRandomCity() {
        try {
            Path path = Paths.get("./data", "cities.txt");
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            int index = (int) (Math.random() * 11);

            return lines.get(index);
        } catch (IOException e) {
            return null;
        }
    }


    public LocalDate getRandomDay() {
        LocalDate randomDate = createRandomDate();
        return randomDate;
    }


    public static int createRandomIntBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public static LocalDate createRandomDate() {
        int day = createRandomIntBetween(21, 30);
        int month = createRandomIntBetween(3, 3);
        return LocalDate.of(2020, month, day);
    }

    public int getRandomSeats() {
        int seats = (int) (Math.random() * 50 + 1);
        return seats;

    }

    public Flight getFlight(int id) throws Exception {

        return flightDao.get(id).get();
    }

    public String getFrom(){
        return getStringDataAsLine("From\t:\t");
    }

    public String getDestination(){
        return  getStringDataAsLine("Destination\t:\t");
    }

    public LocalDate getFlightDate(){
        String date = getStringDataAsLine("Date(yyyy-MM-dd)\t:\t");
        try{
            return LocalDate.parse(date);
        }catch (Exception e){
            System.out.println("Be careful with date format..You  have to enter yyyy-MM-dd");
            return null;
        }
    }

    public List<Flight> getAllFlightByParameter(String from, String to, LocalDate date) throws Exception {
        LogUtility.addLog("FLIGHT SERVICE", "getAllFlightByParameter", LogUtility.LogTypes.INFO);

//        scanner.nextLine();
        List<Flight> flightList = ((FlightDao) flightDao).get(from, to, date);
//        if (flightList.isEmpty()) {
//            System.out.println("Oh Im sorry :( \n" +
//                    "No direct flight has found with given criteria...but u can use this flights instead..");
//            flightList = ((FlightDao) flightDao).getThird(from, to, LocalDate.parse(date));
//        }
        return flightList;
    }

    private Optional<Flight> getFromTo(String from, String to, LocalDate date) {
        return getAllFlight().stream().filter(flight -> flight.getFrom().equals(from)
                && flight.getTo().equals(to)
                && (flight.getDate().equals(date) || flight.getDate().compareTo(date) > 0)).findFirst();
    }

    private List<Flight> getFromList(String from, LocalDate date) {
        return getAllFlight().stream().filter(flight -> flight.getFrom().equals(from)
                && (flight.getDate().equals(date) || flight.getDate().compareTo(date) > 0)).collect(Collectors.toList());
    }

    public Optional<ConnectingFlights> getConnectingFlight(String from, String to, LocalDate date) {
        List<Flight> fromList = getFromList(from, date);
        if (fromList.isEmpty()) {
            return Optional.empty();
        } else {
            for (Flight flight : fromList) {
                Optional<Flight> fromTo = getFromTo(flight.getTo(), to, date);
                if (fromTo.isPresent()) {
                    ConnectingFlights connectingFlights = new ConnectingFlights();
                    connectingFlights.setFirstFlight(flight);
                    connectingFlights.setSecondFlight(fromTo.get());
                    return Optional.of(connectingFlights);

                }
            }
        }
        return Optional.empty();
    }


    public List<Flight> getAllFlight() {
        return (List<Flight>) flightDao.getAll();
    }

    public List<Flight> getAllFlightToday() throws Exception {
        return ((FlightDao) flightDao).getToday();
    }


    public Optional<Flight> getOneFlightInfoById() throws Exception {
        int flightId = getIntData("id\t:\t");

        return flightDao.get(flightId);

    }

    public boolean update(Flight flight) throws Exception {
        return flightDao.update(flight);
    }

    public void getStartLine() {
        scanner.nextLine();
    }
}
