package com.bookingflight.service;

import com.bookingflight.dao.Dao;
import com.bookingflight.dao.DaoFactory;
import com.bookingflight.dao.iml.UserDao;
import com.bookingflight.entity.Flight;
import com.bookingflight.entity.User;
import com.bookingflight.utility.UserUtility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Optional;

public class UserService implements GetInput {


    private Dao userDao;

    public UserService(){
        userDao = DaoFactory.getDAO(DaoFactory.DaoNames.USER);
    }



    public  void createUser(User user) throws  Exception{
        userDao.create(user);
    }


    public boolean login(String username,String password) throws  Exception{

        Optional<User> user = Optional.empty();
        try {
            user = ((UserDao)userDao).get(username, password);
        user.ifPresent(value -> UserUtility.getInstance().setLoggedUser(value));
        } catch (Exception e) {
            System.out.println("\nOops...wrong password or username...\nPlease try again or create a new account :) \n");
        }

        return user.isPresent();
    }


    public void write(Collection<Flight> data) {
        try {
            File file = new File("users.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Collection<User> getAll()throws  Exception {
        return userDao.getAll();
    }
}
