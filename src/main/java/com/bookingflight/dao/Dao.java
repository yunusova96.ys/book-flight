package com.bookingflight.dao;

import com.bookingflight.entity.Booking;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Predicate;

public interface Dao<A> {
    Optional<A> get(String id);
    Optional<A> get(int id);
    Collection<A> getAll();
    Collection<A> getAllBy(Predicate<A> p);
    void create(A data);
    boolean delete(int id);
    boolean update(A a);

    default Collection<A> getAll(String fileName){
        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = ois.readObject();
            ois.close();
            fis.close();
            return (ArrayList<A>) obj;
        } catch (IOException | ClassNotFoundException e) {
            create(fileName,new ArrayList<>());
//            System.out.println(e);
            return new ArrayList<>();
        }
    }

    default void create(String fileName, Collection<A> all){
        try{
            File file = new File(fileName);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(all);
            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
