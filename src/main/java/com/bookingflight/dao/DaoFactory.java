package com.bookingflight.dao;

import com.bookingflight.dao.iml.BookingDao;
import com.bookingflight.dao.iml.FlightDao;
import com.bookingflight.dao.iml.UserDao;

public class DaoFactory {
    public enum DaoNames{
        BOOKING,FLIGHT,USER;
    }

    public static Dao getDAO(DaoNames daoName){
        switch (daoName){
            case USER   :   return new UserDao();
            case FLIGHT :   return new FlightDao();
            case BOOKING:   return new BookingDao();
            default     :   return null;
        }
    }
}
