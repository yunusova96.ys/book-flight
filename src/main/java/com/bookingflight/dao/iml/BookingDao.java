package com.bookingflight.dao.iml;

import com.bookingflight.dao.Dao;
import com.bookingflight.entity.Booking;
import com.bookingflight.utility.UserUtility;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BookingDao implements Dao<Booking> {

    private final String fileName = "booking.txt";

    @Override
    public Optional<Booking> get(String id) {
        return Optional.empty();
    }

    @Override
    public Optional<Booking> get(int id) {
        System.out.println("id is:" + id);
        return getAllByLoggedUser().stream().filter(f -> f.getId() == id).findFirst();
    }

    @Override
    public Collection<Booking> getAll() {
        return getAll(fileName);
    }

    public Collection<Booking> getAllByLoggedUser() {
        int loggedId = UserUtility.getInstance().getLoggedUser().getUserId();
        return getAllBy(booking -> booking.getUserId() == loggedId);

    }

    @Override
    public Collection<Booking> getAllBy(Predicate<Booking> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void create(Booking data) {
        Collection<Booking> all = getAll();
        all.add(data);
        create(fileName, all);
    }


    @Override
    public boolean delete(int id) {
        boolean present = get(id).isPresent();
        Collection<Booking> all = getAllByLoggedUser().stream().filter(f -> f.getId() != id).collect(Collectors.toList());
        create(fileName, all);
        return present;
    }

    @Override
    public boolean update(Booking booking) {
        return false;
    }
}
