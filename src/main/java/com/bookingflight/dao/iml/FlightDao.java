package com.bookingflight.dao.iml;

import com.bookingflight.dao.Dao;
import com.bookingflight.entity.Flight;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FlightDao implements Dao<Flight> {
    private final String fileName = "flights.txt";

    @Override
    public Optional<Flight> get(String id) {
        return Optional.empty();
    }

    @Override
    public Optional<Flight> get(int id) {
        if (id == 0) {
            return Optional.empty();
        }
        return getAll().stream().filter(f -> f.getFlightId() == id).findFirst();
    }


    public List<Flight> get(String from, String to, LocalDate date) {
        List<Flight> list = (List<Flight>)
                getAllBy(flight -> flight.getFrom()
                        .equals(from) && flight.getTo()
                        .equals(to) && flight.getDate()
                        .equals(date));
        if ((list == null) || list.isEmpty()) {
            list = new ArrayList<>();
            return list;
        } else {
            return list;
        }

    }

    public List<Flight> getThird(String from, String to, LocalDate date) {
        List<Flight> list = (List<Flight>)
                getAllBy(flight -> flight.getFrom()
                        .equals(from) || flight.getTo()
                        .equals(to) && flight.getDate()
                        .equals(date));
        if ((list == null) || list.isEmpty()) {
            list = new ArrayList<>();
            return list;
        } else {
            return list;
        }

    }









    public List<Flight> getToday() {//todo 24hours
        List<Flight> list = (List<Flight>)
                getAllBy(flight -> flight.getDate().equals(LocalDate.now()));
        if ((list == null) || list.isEmpty()) {
            list = new ArrayList<>();
            return list;
        } else {
            return list;
        }

    }

    @Override
    public Collection<Flight> getAll() {
        return getAll(fileName);
    }

    @Override
    public Collection<Flight> getAllBy(Predicate<Flight> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void create(Flight data) {
        Collection<Flight> all = getAll();
        all.add(data);
        create(fileName, all);
    }

    @Override
    public boolean update(Flight flight) {
        Collection<Flight> all = getAll();
        boolean done = false;
        for (Flight flight1 : all) {
            if (flight1.getFlightId() == flight.getFlightId()) {
                flight1.setSeats(flight.getSeats());
                done = true;
            }
        }
        create(fileName, all);
        return done;
    }

    @Override
    public boolean delete(int id) {
        boolean present = get(id).isPresent();
        Collection<Flight> all = getAll().stream().filter(f -> f.getFlightId() != id).collect(Collectors.toList());
        create(fileName, all);
        return present;
    }


}
