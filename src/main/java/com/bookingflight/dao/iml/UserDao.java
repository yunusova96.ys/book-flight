package com.bookingflight.dao.iml;

import com.bookingflight.dao.Dao;
import com.bookingflight.entity.User;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UserDao implements Dao<User> {
    private  final String fileName = "users.txt";

    @Override
    public Optional<User> get(String id) {
        return Optional.empty();

    }

    public Optional<User> get(String username, String password) throws Exception{
        List<User> list = (List<User>)getAllBy(user -> user.getUserName().equals(username)&&user.getPassword().equals(password));
        return Optional.of(list.get(0));
    }

    @Override
    public Optional<User> get(int id)
    {
        return getAll().stream().filter(f -> f.getUserId() == id).findFirst();
    }

    @Override
    public Collection<User> getAll() {
      return getAll(fileName);
    }

    @Override
    public Collection<User> getAllBy(Predicate<User> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void create(User data) {
        Collection<User> all = getAll();
        all.add(data);
        create(fileName,all);
    }

    @Override
    public boolean delete(int id) {
        Collection<User> all = getAll().stream().filter(f -> f.getUserId() != id).collect(Collectors.toList());
        create(fileName,all);
        return get(id).isPresent();
    }

    @Override
    public boolean update(User user) {
        return false;
    }


}
