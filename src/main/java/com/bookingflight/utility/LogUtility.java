package com.bookingflight.utility;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LogUtility {

    public enum LogTypes{
        ERROR("error.log"),INFO("info.log");

        private String log;

        LogTypes(String log){
            this.log = log;
        }

        public String getLogName(){ return log;}
    }

    public static void addLog(String logTitle, String logMessage, LogTypes logType){
        Charset utf8 = StandardCharsets.UTF_8;

        try {

            List<String> list = new ArrayList<>();
            list.add(logTitle);
            list.add(LocalDateTime.now().toString());
            list.add(logMessage);
            list.add("****************************************");

            Files.write(Paths.get(logType.getLogName()), list, utf8,
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }


}
