package com.bookingflight.utility;

import com.bookingflight.entity.User;

public class UserUtility {
    private static UserUtility instance;
    private User loggedUser;

    public static UserUtility getInstance(){
        if(instance==null){
            instance = new UserUtility();
        }
        return instance;
    }

    public User getLoggedUser(){
        return loggedUser;
    }

    public void setLoggedUser(User user){
        this.loggedUser = user;
    }
}
