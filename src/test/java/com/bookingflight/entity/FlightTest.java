package com.bookingflight.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class FlightTest {
    Flight flight;

    @BeforeEach
    void setUp() {
         flight = new Flight(1, LocalDate.of(2020, 03, 27)
                , 15, "Baku,Azerbaijan", "Kiev,Ukraine");

    }

    @Test
    void show() {
        System.out.println( flight.show());
    }

    @Test
    void testToString() {
        System.out.println(flight.toString());
    }

    @Test
    void getFlightId() {
        flight.setFlightId(66);
        assertEquals(66,  flight.getFlightId());
    }



    @Test
    void getDate() {
        LocalDate date = flight.getDate();
        assertNotEquals(null,date);
    }



    @Test
    void getSeats() {
        flight.setSeats(5);
        assertEquals(5,flight.getSeats());
    }


    @Test
    void getFrom() {
        flight.setFrom("Wuhan");
        assertTrue("Wuhan".equals(flight.getFrom()));
    }


    @Test
    void getTo() {
        flight.setTo("Hell");
        assertTrue("Hell".equals(flight.getTo()));

    }


}