package com.bookingflight.entity;

import com.bookingflight.service.BookingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {
    Booking booking;
    User user;

    @BeforeEach
    void setUp() {
        user = new User("suzy", "123");
        Passenger passenger1 = new Passenger("c", "d");
        Passenger passenger2 = new Passenger("e", "f");
        Passenger passenger3 = new Passenger("g", "h");
        Flight flight2 = new Flight(1, LocalDate.of(2020, 03, 27)
                , 15, "Baku,Azerbaijan", "Kiev,Ukraine");
        List<Passenger> passengerList = new ArrayList<>();
        passengerList.add(passenger1);
        passengerList.add(passenger2);
        passengerList.add(passenger3);
         booking = new Booking(2, flight2.getFlightId(), user.getUserId(), passengerList);

    }


    @Test
    void show() {
        System.out.println(booking.show());
    }

    @Test
    void getId() {
        assertNotEquals(0,booking.getId());
    }

    @Test
    void setId() {
        booking.setId(5);
        assertEquals(5,booking.getId());
    }

    @Test
    void getFlightId() {
        assertNotEquals(0,booking.getFlightId());
    }

    @Test
    void setFlightId() {
        booking.setFlightId(55);
        assertEquals(55,booking.getFlightId());
    }

    @Test
    void getUserId() {
        booking.setUserId(5);
        assertNotEquals(0,booking.getUserId());
    }

    @Test
    void setUserId() {
        booking.setUserId(5);
        assertEquals(5,booking.getUserId());
    }

    @Test
    void getPassengerList() {
        booking.getPassengerList().size();
        assertNotEquals(0,booking.getPassengerList().size());
    }


}