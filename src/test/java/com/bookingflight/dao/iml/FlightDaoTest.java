package com.bookingflight.dao.iml;

import com.bookingflight.entity.Flight;
import com.bookingflight.entity.User;
import com.bookingflight.service.FlightService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightDaoTest {
    FlightDao flightDao;
    Flight flight;

    @BeforeEach
    void setUp() {
        flightDao = new FlightDao();
    }

    @Test
    void getAll() {
        Collection<Flight> flights = flightDao.getAll();
        assertNotEquals(0, flights.size());
    }

    @Test
    void get() {
        flight = new Flight(1,
                LocalDate.of(2020,03,27),
                12,"BAki","Wuhan");
        flightDao.create(flight);
       assertNotEquals(null, flightDao.get(1));
    }


    @Test
    void getToday() {
        List<Flight> today = flightDao.getToday();
        assertNotEquals(0,today.size());
    }

    @Test
    void testGetAll() {
        Collection<Flight> all = flightDao.getAll();
        assertNotEquals(0,all.size());
    }


    @Test
    void create() {
        flight = new Flight(1,
                LocalDate.of(2020,03,27),
                12,"BAki","Wuhan");
        flightDao.create(flight);
        assertNotEquals(null, flightDao.get(1));
    }

}
