package com.bookingflight.service;

import com.bookingflight.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {
    UserService userService;
    User user;

    @BeforeEach
    void setUp() {
        userService = new UserService();
    }


    @Test
    void getAll() throws Exception {
        Collection<User> all = userService.getAll();
        assertNotEquals(null, all);
    }

    @Test
    void createUser() throws Exception {
        user = new User("suzy", "123");
        userService.createUser(user);
        assertNotEquals(0, userService.getAll().size());

    }

    @Test
    void login() throws Exception {
        user = new User("suzy", "123");
        userService.createUser(user);
        String username = "suzy";
        String password = "123";
        assertTrue(userService.login(username, password));
    }



}