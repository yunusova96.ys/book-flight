package com.bookingflight.service;

import com.bookingflight.dao.iml.BookingDao;
import com.bookingflight.entity.Booking;
import com.bookingflight.entity.Flight;
import com.bookingflight.entity.Passenger;
import com.bookingflight.entity.User;
import com.bookingflight.utility.UserUtility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class BookingServiceTest {
    BookingService bookingService;
    User user;
    Flight flight;
    Booking booking;
    BookingDao bookingDao;

    @BeforeEach
    void setUp() {
        bookingDao = new BookingDao();
//        booking = new Booking();
        bookingService = new BookingService();
        user = new User("suzy", "123");
        Passenger passenger1 = new Passenger("c", "d");
        Passenger passenger2 = new Passenger("e", "f");
        Passenger passenger3 = new Passenger("g", "h");
        flight = new Flight(1, LocalDate.of(2020, 03, 27)
                , 20, "Baku,Azerbaijan", "Wuhan,China");
        Flight flight2 = new Flight(1, LocalDate.of(2020, 03, 27)
                , 15, "Baku,Azerbaijan", "Kiev,Ukraine");
        List<Passenger> passengerList = new ArrayList<>();
        passengerList.add(passenger1);
        passengerList.add(passenger2);
        passengerList.add(passenger3);
        Booking booking1 = new Booking(1, flight.getFlightId(), user.getUserId(), passengerList);
        Booking booking2 = new Booking(2, flight2.getFlightId(), user.getUserId(), passengerList);
        bookingDao.create(booking1);
    }

    @Test
    void createBooking() {
        user = new User("suzy", "123");
        Passenger passenger1 = new Passenger("c", "d");
        Passenger passenger2 = new Passenger("e", "f");
        Passenger passenger3 = new Passenger("g", "h");
        flight = new Flight(1, LocalDate.of(2020, 03, 27)
                , 20, "Baku,Azerbaijan", "Wuhan,China");
        Flight flight2 = new Flight(1, LocalDate.of(2020, 03, 27)
                , 15, "Baku,Azerbaijan", "Kiev,Ukraine");
        List<Passenger> passengerList = new ArrayList<>();
        passengerList.add(passenger1);
        passengerList.add(passenger2);
        passengerList.add(passenger3);

        assertTrue(bookingService.createBooking(2, 2, user, passengerList));
    }



}