package com.bookingflight.service;

import com.bookingflight.entity.Flight;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FlightServiceTest {
    FlightService flightService;


    @BeforeEach
    void setUp() {
        flightService = new FlightService();
    }

    @Test
    void createAllFlights() {
        flightService.createAllFlights();
        List<Flight> allFlight = flightService.getAllFlight();
        assertNotEquals(0,allFlight.size());
    }

    @Test
    void createRandomFlights() {
        Flight randomFlights1 = flightService.createRandomFlights();
        Flight randomFlights2 = flightService.createRandomFlights();
        assertNotEquals(randomFlights1, randomFlights2);
    }

    @Test
    void getRandomFlightId() {
        int id1 = flightService.getRandomFlightId();
        int id2 = flightService.getRandomFlightId();
        assertNotEquals(id1,id2);
    }

    @Test
    void getRandomCity() {
        String city1 = flightService.getRandomCity();
        String city2 = flightService.getRandomCity();
        assertFalse(city1.equals(city2.toString()));
    }

    @Test
    void getRandomDay() {
        LocalDate day1 = flightService.getRandomDay();
        LocalDate day2 = flightService.getRandomDay();
        assertFalse(day1.equals(day2.toString()));
    }


    @Test
    void getRandomSeats() {
        int seat1 = flightService.getRandomSeats();
        int seat2 = flightService.getRandomSeats();
        assertNotEquals(seat1,seat2);
    }





    @Test
    void getAllFlight() {
        List<Flight> allFlight = flightService.getAllFlight();
        assertNotEquals(0,allFlight.size());

    }

    @Test
    void getAllFlightToday() throws Exception {
        List<Flight> allFlight = flightService.getAllFlightToday();
        assertNotEquals(0,allFlight.size());
    }




}